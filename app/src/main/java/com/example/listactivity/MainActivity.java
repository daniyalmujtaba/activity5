package com.example.listactivity;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Typeface;
import android.os.Bundle;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private ListView listView;
    private String teamName[] = {
            "Karachi Kings",
            "Lahore Qalandars",
            "Multan Sultans",
            "Peshawar Zalmi",
            "Islamabad United",
            "Quetta Gladiators"
    };

    private Integer imageid[] = {
            R.drawable.karachi,
            R.drawable.lahore,
            R.drawable.multan,
            R.drawable.peshawar,
            R.drawable.isb,
            R.drawable.quetta

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        TextView textView = new TextView(this);
        textView.setTypeface(Typeface.DEFAULT_BOLD);
        textView.setText("List of Teams");

        ListView listView=(ListView)findViewById(android.R.id.list);
        listView.addHeaderView(textView);

        // For populating list data
        TeamList teamList = new TeamList(this, teamName, imageid);
        listView.setAdapter(teamList);


    }

}