package com.example.listactivity;

import android.widget.ArrayAdapter;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;


public class TeamList extends ArrayAdapter {
    private String[] teamName;
    private Integer[] ImageId;
    private Activity context;


    public TeamList(Activity  context, String[] teamName,Integer[] ImageId) {
        super(context, R.layout.row,teamName);
        this.context = context;
        this.teamName = teamName;
        this.ImageId = ImageId;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row=convertView;
        LayoutInflater inflater = context.getLayoutInflater();
        if(convertView==null)
            row = inflater.inflate(R.layout.row, null, true);
        TextView textViewTeam = (TextView) row.findViewById(R.id.textViewTeamName);
        ImageView imageFlag = (ImageView) row.findViewById(R.id.imageViewLogo);
        textViewTeam.setText(teamName[position]);
        imageFlag.setImageResource(ImageId[position]);
        return  row;
    }
}
